import React from 'react';
import Header from './components/Header';
import Welcome from './components/Welcome';
import Events from './components/Events';
import Caraousel from './components/Caraousel.js';
import Footer from './components/Footer.js';

export default function App() {

  return (
    <div className='container'>
      <div className="one">
        <Header/>
        <Welcome/>
      </div>
      <div className="two">
        <Caraousel/>
      </div>
      <div className="three">
        <Footer/>
      </div>
    </div>
  )
}


