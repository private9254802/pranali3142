import React from 'react'
import './Welcome.css';
import { Button } from 'react-bootstrap';

const Welcome = () => {
  return (
    <div className='showcase'>
      <div className="overlay">
        <div className="showcase-content " >
          <h1>Welcome to Pranali</h1>
        </div>
        <div className="showcase-content-2">
          <p>Hola Amigos !! Let's Dive into the wonderful Journey of Rotaract</p>
          <Button variant="primary" href="login.js">
            Get Started
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Welcome
