import React, { useEffect, useState } from 'react';
import './Caraousel.css';
import aara2 from '../assets/aara2.jpg';
import RISE4 from '../assets/RISE4.jpg'
import DAPTAR from '../assets/DAPTAR.jpg'
import ifinance from '../assets/I Finance 2.jpg'


const Caraousel = () => {
    const[slideIndex, setSlideIndex] = useState(1);  //state to control and render 

    useEffect(() => {
        showSlides(slideIndex);
    }, [slideIndex]);

    
    function plusSlides(n) {  //function for next and previous slides
        const newSlideIndex = slideIndex + n; 
        setSlideIndex(newSlideIndex);
    }

    function currentSlide(n) {   //for thumbnail
      setSlideIndex(n);
    }

    function showSlides(n) {
        let i;
        let slides = document.getElementsByClassName("mySlides");
        let dots = document.getElementsByClassName("dot");
    
        if (!slides || !dots || slides.length === 0 || dots.length === 0) {
            return;
        }
    
        if (n > slides.length) {
            setSlideIndex(1);
        }
        if (n < 1) {
            setSlideIndex(slides.length);
        }
    
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
    
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
    
        if (slides[slideIndex - 1]) {
            slides[slideIndex - 1].style.display = "block";
        }
        if (dots[slideIndex - 1]) {
            dots[slideIndex - 1].className += " active";
        }
    };
   
  return (
    <>
        <div className="slideshow-container">
            <div className="slideshow-container-text">
                Glimpse Of Events
            </div>
                <div className="mySlides fade">
                    <div className="numbertext">1 / 4</div>
                    <div className="image-overlay">
                        <img src={aara2} alt='AARA'/>
                    </div>
                    <div className="text">Academy For Awarding Rotaract Achievement (AARA)</div>
                </div>

                <div className="mySlides fade">
                    <div className="numbertext">2 / 4</div>
                    <div className="image-overlay">
                        <img src={RISE4} alt='RISE'/>
                    </div>
                    <div className="text">Rotaract Initiative for Sporting Excellence (RISE)</div>
                </div>

                <div className="mySlides fade">
                    <div className="numbertext">3 / 4</div>
                    <img src={DAPTAR}  alt='DAPTAR'/>
                    <div className="text">DAPTAR</div>
                </div>

                <div className="mySlides fade">
                    <div className="numbertext">4 / 4</div>
                    <img src={ifinance}  alt='ifinance'/>
                    <div className="text">I Finance </div>
                </div>
            <button className="prev"  onClick={() => plusSlides(-1)}>&#10094;</button>
            <button className="next" onClick={() => plusSlides(1)}>&#10095;</button>
        </div>
        {/* <br/> */}

        <div className='textcenter'>
            <span className="dot" onClick={() => currentSlide(1)}></span>
            <span className="dot" onClick={() => currentSlide(2)}></span>
            <span className="dot" onClick={() => currentSlide(3)}></span>
        </div>
    </>
  );
};

export default Caraousel;
