import React from 'react'
import './Header.css'
import logo from '../assets/rc3142_logo.png'

const Header = () => {
  return (  
    <div className="navbar">
        <div className="logo">
            <img src={logo} alt="Logo"/>
        </div>
        <nav>
            <ul>
                <li><a href="1">Home</a></li>
                <li><a href="2">About</a></li>
                <li><a href="3">Contact</a></li>
                <li><a href="4">DashBoard</a></li>
            </ul>
        </nav>
    </div>
    
        
  )
}

export default Header
