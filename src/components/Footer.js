import React from 'react'
import ReactDOM from 'react-dom'
import './Footer.css'
import '@fortawesome/fontawesome-svg-core'
import logo from './images/rc3142_logo.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core';
import { faLinkedin, faInstagram, faFacebook } from '@fortawesome/free-brands-svg-icons';


const Footer = () => {
  return (
    <>
      <footer className="footer-container">
        <div className="footer-section-1">
          <img src={logo} alt="RC Logo" />
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure magnam mollitia a consectetur, voluptas quaerat, minus non tenetur, maiores libero nam placeat facilis soluta quam consequatur. Natus repellat aspernatur et.</p>
        </div>

        <div className="footer-section-2">
          <h2>Email NewsLetter</h2>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Similique nisi delectus</p>
          <input type="email" placeholder='Enter Email here' />
          <button type='submit'> Submit </button>
        </div>

        <div className="footer-section-3">
          <h2>Follow Us On Social Media</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, libero!</p>

          <div className="social-icons">
          <a  className='linkedin-icon' href="https://in.linkedin.com/company/rotaract-district-3142" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faLinkedin}/>
            </a>
            <a  className="instagram-icon"  href="https://www.instagram.com/rotaract3142" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faInstagram}/>
            </a>
            <a className="facebook-icon" href="https://www.facebook.com/profile.php?id=100067029652035" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faFacebook}/>
            </a>
          </div>
        </div>
        <div className="footer-section-4">
          <p>CopyRight © 2023, ALl Rights Reserved.</p>
        </div>
      </footer>
    </>
  )
}

export default Footer;


